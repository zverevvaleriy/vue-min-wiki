import Vue from 'vue'
import Vuex from 'vuex'
import slugify from 'slugify'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    articles: null
  },
  mutations: {
    setArticles (state, articles) {
      state.articles = articles
    },
    removeArticle (state, name) {
      let articles = { ...state.articles }
      delete articles[name]
      this.state.articles = articles
    },
    updateArticle (state, article) {
      state.articles[article.name] = article
    }
  },
  actions: {
    loadArticles ({ state, commit }) {
      if (state.articles !== null) {
        return
      }
      try {
        commit('setArticles', JSON.parse(localStorage['wiki_articles']))
      } catch (e) {
        state.articles = {}
        localStorage.setItem('wiki_articles', '{}')
      }
    },
    saveArticle ({ state, commit }, article) {
      return new Promise((resolve, reject) => {
        commit('updateArticle', article)
        localStorage.setItem('wiki_articles', JSON.stringify(state.articles))
        resolve(article)
      })
    },
    deleteArticle ({ state, commit }, name) {
      return new Promise((resolve, reject) => {
        commit('removeArticle', name)
        localStorage.setItem('wiki_articles', JSON.stringify(state.articles))
        resolve()
      })
    },
    createArticle ({ state, dispatch }, title) {
      return new Promise((resolve, reject) => {
        let slug = slugify(title)
        let article = {
          name: slug,
          title: title,
          text: '',
          created_at: +new Date()
        }
        if (state.articles[slug] !== undefined) {
          let count = 1
          while (state.articles[slug + '-' + count] !== undefined) {
            count += 1
          }
          article.name = slug + '-' + count
        }
        dispatch('saveArticle', article).then(resolve).catch(reject)
      })
    }
  },
  getters: {
    getList (state) {
      return Object.values(state.articles || {})
    },
    getArticle (state) {
      return (name) => {
        return state.articles[name] || null
      }
    }
  }
})
