import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Button from './components/Button.vue'
import TextField from './components/TextField.vue'

Vue.config.productionTip = false

Vue.component('w-btn', Button)
Vue.component('w-text-field', TextField)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
