import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/edit/:name',
      name: 'edit',
      component: () => import(/* webpackChunkName: "about" */ './views/Edit.vue')
    },
    {
      path: '/view/:name',
      name: 'view',
      component: () => import(/* webpackChunkName: "about" */ './views/View.vue')
    },
    {
      path: '/404',
      name: 404,
      component: () => import(/* webpackChunkName: "about" */ './views/PageNotFound.vue')
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})
