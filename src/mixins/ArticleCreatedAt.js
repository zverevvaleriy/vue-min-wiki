function pad (v, size = 2, char = '0') {
  var t = v.toString()
  while (t.length < size) t = char + t
  return t
}

export default {
  computed: {
    createdAt () {
      let date = new Date(this.article.created_at)
      return `${pad(date.getDate())}-${pad(date.getMonth() + 1)}-${date.getFullYear()} ${pad(date.getHours())}:${pad(date.getMinutes())}`
    }
  }
}
